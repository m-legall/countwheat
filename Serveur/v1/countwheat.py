import torch
from PIL import Image
import os
#import cv2 as cv
#os.chdir("C:/Users/m38le/Documents/countwheat")

# Model
model = torch.hub.load('ultralytics/yolov5', 'custom', path='C:\\Users\\matte\\Documents\\ECL\\PE n°63\\Serv_fastapi\\best.pt')
model.conf = 0.4
# Images

# img1 = Image.open('C:\\Users\\matte\\Documents\\ECL\\PE n°63\\Serv_fastapi\\0eed0a689.jpg')  # PIL image
# img2 = Image.open('C:\\Users\\matte\\Documents\\ECL\\PE n°63\\Serv_fastapi\\0ebac9a23.jpg')
# imgs = [img1,img2]  # batch of images

# Inference
def countwheat(path):
	img = Image.open(path)
	imgs = [img]
	results = model(imgs, size=416)  # includes NMS
	return results,len(results.xyxy[0])

# # Results
# results.print()
# results.show()  # or .show()
# print("a")
# results.xyxy[0]  # img1 predictions (tensor)
# print("b")
# print(len(results.pandas().xyxy[0]))