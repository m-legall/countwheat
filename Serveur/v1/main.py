from fastapi import FastAPI, File, Request, UploadFile
from fastapi.responses import FileResponse
from fastapi.responses import JSONResponse
from countwheat import countwheat
import uvicorn
from pydantic import BaseModel
app = FastAPI()

@app.get("/")
def msg():
	return "/get_epis/{image_path}?image=[LIEN]"

@app.get("/get_epis/{image_path}")
async def get_epis(image):
	#frame = cv.imread(image)
	print(image)
	#if image[:7]=="file://" : image = image[7:]
	nb = countwheat(image)
	res = {"Nombre d'épis": nb[1]}
	nb[0].save()
	return res

# class Analyzer(BaseModel):
#     name: str
#     img_dimensions: str
#     encoded_img: str

def transform_img(frame):
	### A modifier pour rogner, changer contraste, ...
	return frame

# Pour tester
if __name__=='__main__':
	uvicorn.run(app,host='127.0.0.1', port=8000)

# Pour tester : lancer ça
# http://127.0.0.1:8000/get_epis/{image_path}?image=LIEN DE L'IMAGE