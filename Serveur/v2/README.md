# yolov5-fastapi
Machine Learning Model API using YOLOv5 with FAST API

### Getting start for this project

Pour lancer l'application :
```
uvicorn main:app --reload --host 0.0.0.0 --port 8000 
```

On peut y acceder via localhost:port/docs
(ex: 127.0.0.1:8080/docs)
Les différents fonctions permettent d'obtenir le nombre d'épis / l'image renvoyée et les résultats 1 par 1 avec les localisation des épis détectés en json