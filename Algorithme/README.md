### Explications

Beaucoup de code a été fait sur google colab, notamment pour entraîner les réseaux car c'était beaucoup (BEAUCOUP) plus rapide et cela permettait d'utiliser l'ordi pendant l'entrainement. La  version présente n'est donc pas nécessairement exacte a celle utilisée.

La versions ici doit fonctionner aussi et c'est très simple de la transposer sur colab, il faut juste ajouter une partie pour importer le jeu de données.

Le jeu de test est important pour obtenir les courbes PR/F1/... mais les divisions (70%/15%/15%) sont arbitraires