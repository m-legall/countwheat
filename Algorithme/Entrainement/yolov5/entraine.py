import torch
from IPython.display import Image  # for displaying images
import os 
import random
import shutil
from sklearn.model_selection import train_test_split
import xml.etree.ElementTree as ET
from xml.dom import minidom
from tqdm import tqdm
from PIL import Image, ImageDraw
import numpy as np
import matplotlib.pyplot as plt

random.seed(108)

path = "C:\\Users\\matte\\Documents\\ECL\\PE n°63\\pe-63-countwheat\\data"

imgs_train = []
for f in glob.glob(path+"\\images\\train"):
    im = Image.open(f)
    imgs_train.append(im)

imgs_valid = []
for f in glob.glob(path+"\\images\\valid"):
    im = Image.open(f)
    imgs_valid.append(im)

$ py train.py --img 1024 --batch 64 --epochs 30 --data countwheat.yaml --weights yolov5s.pt --workers 24 --name yolo_countwheat
