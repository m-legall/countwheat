# PE63 : Countwheat

Repo contenant l'essentiel des éléments du PE-63 2021-2022

/Algorithme pour les programmes d'entraînement et quelques exemples de résultats

/Images pour le jeu d'entraînement avec les labels dans le bon format

/Rapports avec le rapport comparatif et le rapport final

/Serveur avec les éléments essentiels liés au serveur codé avec le framework FastAPI